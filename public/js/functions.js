/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function insertAtCursor(myField, myValue) {

       myField.title = '';
        
        //IE support
        if (document.selection) 
        {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
        }
        else if (myField.selectionStart || myField.selectionStart == '0') 
        {
            var startPos = myField.selectionStart;
            var endPos = myField.selectionEnd;
            myField.value = myField.value.substring(0, startPos) + myValue + myField.value.substring(endPos, myField.value.length);

        } 
        else 
        {
            myField.value += myValue;
        }
     
 }
 
 
  function trim(sValue)
    {
        if(sValue)
        {
            return sValue.replace(/^\s+|\s+$/g, "");
        }
        else
        {
            return '';
        }
    }
        
function validateEmail(email) 
{
    var re = /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/;
    return re.test(email);
}


function ucwords(str,force){
  str=force ? str.toLowerCase() : str;  
  return str.replace(/(\b)([a-zA-Z])/g,
           function(firstLetter){
              return   firstLetter.toUpperCase();
           });
}



//For Monday time picker..
function tpStartOnHourShowCallbackMonShift(hour) {
    
    var tpEndHour = $('#MondayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#MondayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackMonShift(hour, minute) {
    
    
    var tpEndHour = $('#MondayEndShift').timepicker('getHour');
    var tpEndMinute = $('#MondayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#MondayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackMonShift(hour) {
    var tpStartHour = $('#MondayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#MondayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackMonShift(hour, minute) {
    var tpStartHour = $('#MondayStartShift').timepicker('getHour');
    var tpStartMinute = $('#MondayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#MondayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}







//For Tuesday time picker..
function tpStartOnHourShowCallbackTueShift(hour) {
    
    var tpEndHour = $('#TuesdayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#TuesdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackTueShift(hour, minute) {
    
    
    var tpEndHour = $('#TuesdayEndShift').timepicker('getHour');
    var tpEndMinute = $('#TuesdayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#TuesdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackTueShift(hour) {
    var tpStartHour = $('#TuesdayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#TuesdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackTueShift(hour, minute) {
    var tpStartHour = $('#TuesdayStartShift').timepicker('getHour');
    var tpStartMinute = $('#TuesdayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#TuesdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}






//For Wednesday time picker..
function tpStartOnHourShowCallbackWedShift(hour) {
    
    var tpEndHour = $('#WednesdayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#WednesdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackWedShift(hour, minute) {
    
    
    var tpEndHour = $('#WednesdayEndShift').timepicker('getHour');
    var tpEndMinute = $('#WednesdayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#WednesdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackWedShift(hour) {
    var tpStartHour = $('#WednesdayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#WednesdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackWedShift(hour, minute) {
    var tpStartHour = $('#WednesdayStartShift').timepicker('getHour');
    var tpStartMinute = $('#WednesdayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#WednesdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}








//For Thursday time picker..
function tpStartOnHourShowCallbackThuShift(hour) {
    
    var tpEndHour = $('#ThursdayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#ThursdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackThuShift(hour, minute) {
    
    
    var tpEndHour = $('#ThursdayEndShift').timepicker('getHour');
    var tpEndMinute = $('#ThursdayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#ThursdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackThuShift(hour) {
    var tpStartHour = $('#ThursdayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#ThursdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackThuShift(hour, minute) {
    var tpStartHour = $('#ThursdayStartShift').timepicker('getHour');
    var tpStartMinute = $('#ThursdayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#ThursdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}






//For Friday time picker..
function tpStartOnHourShowCallbackFriShift(hour) {
    
    var tpEndHour = $('#FridayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#FridayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackFriShift(hour, minute) {
    
    
    var tpEndHour = $('#FridayEndShift').timepicker('getHour');
    var tpEndMinute = $('#FridayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#FridayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackFriShift(hour) {
    var tpStartHour = $('#FridayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#FridayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackFriShift(hour, minute) {
    var tpStartHour = $('#FridayStartShift').timepicker('getHour');
    var tpStartMinute = $('#FridayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#FridayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}





//For Saturday time picker..
function tpStartOnHourShowCallbackSatShift(hour) {
    
    var tpEndHour = $('#SaturdayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#SaturdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackSatShift(hour, minute) {
    
    
    var tpEndHour = $('#SaturdayEndShift').timepicker('getHour');
    var tpEndMinute = $('#SaturdayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#SaturdayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackSatShift(hour) {
    var tpStartHour = $('#SaturdayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#SaturdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackSatShift(hour, minute) {
    var tpStartHour = $('#SaturdayStartShift').timepicker('getHour');
    var tpStartMinute = $('#SaturdayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#SaturdayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}




//For Sunday time picker..
function tpStartOnHourShowCallbackSunShift(hour) {
    
    var tpEndHour = $('#SundayEndShift').timepicker('getHour');
    // all valid if no end time selected
    if ($('#SundayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
    
}
function tpStartOnMinuteShowCallbackSunShift(hour, minute) {
    
    
    var tpEndHour = $('#SundayEndShift').timepicker('getHour');
    var tpEndMinute = $('#SundayEndShift').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#SundayEndShift').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
    
}

function tpEndOnHourShowCallbackSunShift(hour) {
    var tpStartHour = $('#SundayStartShift').timepicker('getHour');
    // all valid if no start time selected
    if ($('#SundayStartShift').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackSunShift(hour, minute) {
    var tpStartHour = $('#SundayStartShift').timepicker('getHour');
    var tpStartMinute = $('#SundayStartShift').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#SundayStartShift').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}


function tpStartOnHourShowCallbackLunch(hour) {
    var tpEndHour = $('#LunchPeriodTo').timepicker('getHour');
    // all valid if no end time selected
    if ($('#LunchPeriodTo').val() == '') { return true; }
    // Check if proposed hour is prior or equal to selected end time hour
    if (hour <= tpEndHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpStartOnMinuteShowCallbackLunch(hour, minute) {
    var tpEndHour = $('#LunchPeriodTo').timepicker('getHour');
    var tpEndMinute = $('#LunchPeriodTo').timepicker('getMinute');
    // all valid if no end time selected
    if ($('#LunchPeriodTo').val() == '') { return true; }
    // Check if proposed hour is prior to selected end time hour
    if (hour < tpEndHour) { return true; }
    // Check if proposed hour is equal to selected end time hour and minutes is prior
    if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}

function tpEndOnHourShowCallbackLunch(hour) {
    var tpStartHour = $('#LunchPeriodFrom').timepicker('getHour');
    // all valid if no start time selected
    if ($('#LunchPeriodFrom').val() == '') { return true; }
    // Check if proposed hour is after or equal to selected start time hour
    if (hour >= tpStartHour) { return true; }
    // if hour did not match, it can not be selected
    return false;
}
function tpEndOnMinuteShowCallbackLunch(hour, minute) {
    var tpStartHour = $('#LunchPeriodFrom').timepicker('getHour');
    var tpStartMinute = $('#LunchPeriodFrom').timepicker('getMinute');
    // all valid if no start time selected
    if ($('#LunchPeriodFrom').val() == '') { return true; }
    // Check if proposed hour is after selected start time hour
    if (hour > tpStartHour) { return true; }
    // Check if proposed hour is equal to selected start time hour and minutes is after
    if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
    // if minute did not match, it can not be selected
    return false;
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
    }
   return true;
}


function getUrlVars() {
    var vars = { };
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 40;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
	var rnum = Math.floor(Math.random() * chars.length);
	randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
}


function modAlert(text) {
    $("#modP").remove();
    var html = "<p id='modP'>" + text + "</p>";
    $(html).appendTo("body");
    $("#modP").dialog({
	resizable:  false,
	draggable:  false,
	height:	    "auto",
	modal:	    true,
	title:	    "Alert",
	buttons: {
	    Close: {
		click:	function() {
			    $(this).dialog("destroy");
			},
		"class":	"btnCancel",
		text:	"Close"
	    }
	}
    });	
}
