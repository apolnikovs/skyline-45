<?php

/**
 * Short Description of CustomBusinessModel.
 * 
 * Long description of CustomBusinessModel.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.0
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 ******************************************************************************/
 
abstract class CustomBusinessModel {
   
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    private static $generator = null;

    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    protected $controller;  
    public $debug = false;
    /**#@-*/
       
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param object $controller 
     */
    public function __construct(CustomController $controller) {
        $this->controller = $controller;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    protected function log( $message, $tag = "" ) {
        $this->controller->log( $message, $tag );
    } 
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    protected function loadModel( $model_path ) {
        return $this->controller->loadModel( $model_path );
    }
         
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param int $length 
     * @return string
     */
    public function GenPassword($length = 8) {
        // $length max = 32
        return substr(md5(rand().rand()), 0, $length);
    }
     
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param int $syllables
     * @param boolean $use_prefix
     * @return string
     */
    public function GenReadablePassword($syllables = 3, $use_prefix = false) {
        
        // See http://www.anyexample.com/programming/php/php__password_generation.xml

        // 20 prefixes
        $prefix = array('aero', 'anti', 'auto', 'bi', 'bio',
                        'cine', 'deca', 'demo', 'dyna', 'eco',
                        'ergo', 'geo', 'gyno', 'hypo', 'kilo',
                        'mega', 'tera', 'mini', 'nano', 'duo');

        // 10 random suffixes
        $suffix = array('dom', 'ity', 'ment', 'sion', 'ness',
                        'ence', 'er', 'ist', 'tion', 'or'); 

        // 8 vowel sounds 
        $vowels = array('a', 'o', 'e', 'i', 'y', 'u', 'ou', 'oo'); 

        // 20 random consonants 
        $consonants = array('w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 
                            'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'qu');

        $password = $use_prefix ? $this->RandomArrayElement($prefix) : '';
        $password_suffix = $this->RandomArrayElement($suffix);

        for($i=0; $i<$syllables; $i++) {
            // selecting random consonant
            $doubles = array('n', 'm', 't', 's');
            $c = $this->RandomArrayElement($consonants);
            if (in_array($c, $doubles)&&($i!=0)) { // maybe double it
                if (rand(0, 2) == 1) // 33% probability
                    $c .= $c;
            }
            $password .= $c;
            //

            // selecting random vowel
            $password .= $this->RandomArrayElement($vowels);

            if ($i == $syllables - 1) // if suffix begin with vovel
                if (in_array($password_suffix[0], $vowels)) // add one more consonant 
                    $password .= $this->RandomArrayElement($consonants);

        }

        // selecting random suffix
        $password .= $password_suffix;

        return $password;
    }

    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array &arr 
     * @return array
     */
    private function RandomArrayElement(&$arr) {
        return $arr[rand(0, sizeof($arr)-1)];
    }
    
    /**
     * Return LoremIpsum Generator Class object.
     * 
     * Usage:
     * The only public method in the class is getContent.
     *
     * Description:
     * string getContent( int $wordCount [, string $format = html] [, boolean $loremipsum = true] )
     *
     * Returns the desired amount of content as a string.
     *
     * Parameters:
     *
     * $wordCount
     * The number of words to be returned.
     *
     * $format
     * The output mode, one of ‘html’, ‘txt’, or ‘plain’. HTML by default.
     *
     * HTML: The content is divided into paragraphs, using the paragraph ( <p></p> ) tag.
     * Text: The content is divided into paragraphs with the leading line of each paragraph tabbed
     * Plain: The content is returned unformatted
     *
     * $loremipsum
     * Whether or not the content should begin with ‘Lorem ipsum’. True by default.
     *
     * Example:
     *
     * <code> 
     * // 100 words in html format
     * $text = $this->LoremIpsum()->getContent(100);
     * 
     * // 100 words without any formatting
     * $text = $this->LoremIpsum()->getContent(100, 'plain');
     * 
     * // 100 words with 'text' formatting
     * $text = $this->LoremIpsum()->getContent(100, 'txt');
     * 
     * // 100 words with html format, not beginning with lorem ipsum
     * $text = $this->LoremIpsum()->getContent(100, NULL, false);
     * // or
     * $text = $this->LoremIpsum()->getContent(100, 'html', false);
     * </code>
     *  
     * Additional Notes:
     * 
     * Both the HTML and Text output modes use paragraph formatting. 
     * The mean word count of each paragraph is predetermined and can be set 
     * in the constructor, currently the default is 100. Note that this is the 
     * mean word count, the actual word count for each paragraph will vary in 
     * the same way the length of each sentence will vary.
     * 
     * @return object LoremIpsumGenerator 
     */
    public function LoremIpsum() {
        
        if (self::$generator == null) {
            include_once('LoremIpsum.class.php');
            self::$generator = new LoremIpsumGenerator;
        }
        return self::$generator;
    }
      
}

?>
