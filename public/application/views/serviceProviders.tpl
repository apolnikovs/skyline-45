{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ServiceProviders}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
     <script type="text/javascript" src="{$_subdomain}/js/ajaxfileupload.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
         .fieldLabel{
             width:215px!important;
             }
    </style>
{/block}
{block name=scripts}


<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.placeholder.min.js"></script>
    

<script type="text/javascript">

       function ajaxFileUpload()
    {
           
            
            $.ajaxFileUpload
            (
                    {
                            url:'{$_subdomain}/OrganisationSetup/serviceProviders/uploadImage/'+$('#ServiceProviderID').val(),
                            secureuri:false,
                            fileElementId:'DocLogo',
                            dataType: 'json',
                            data:{ name:'logan', id:'id' }
                            
                    }
            )

            return true;

    }
    
    
    
    $.validator.addMethod(
	"multiEmail",
	function(value, element) {
	    if(this.optional(element)) { // return true on optional element 
		return true;
	    }
	    var emails = value.split(/[;]+/); // split element by ;
	    valid = true;
	    for(var i in emails) {
		value = emails[i];
		valid = valid && $.validator.methods.email.call(this, $.trim(value), element);
	    }
	    return valid;
	},
	$.validator.messages.email
    );    


     var $statuses = [
	{foreach $statuses as $st}
	    ["{$st.Name}", "{$st.Code}"],
	{/foreach}
     ]; 
                    
    var vat_rate_array = [];
    {foreach $countries as $country}
        vat_rate_array[{$country.CountryID}] = [];
        {if isset($vatRates[$country.CountryID])}            
            {foreach $vatRates[$country.CountryID] as $vatRateItem}
                vat_rate_array[{$country.CountryID}][{$vatRateItem.VatRateID}] = '{$vatRateItem.Code}';
            {/foreach}
        {/if}
    {/foreach}

    
    function gotoEditPage($sRow) {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
    }


    function sendSettings($sRow) {
    
	if($sRow[2] == "ServiceBase") {
	
	    $("#processDisplayText").show();

	    $.post("{$_subdomain}/OrganisationSetup/serviceProviders/sendSettings/" + urlencode($sRow[0]), '', function(data) {
		var p = eval("(" + data + ")");
		if(p['status'] == "OK") {
		    $("#processDisplayText").hide();
		    alert(p['message']); 
		} else {
		    $("#processDisplayText").hide();
		    alert("{$page['Errors']['settings_sent_fail']|escape:'html'}");
		}
	    });
                 
         } else {
	 
	    alert("{$page['Errors']['servicebase_missing']|escape:'html'}");
	    
         }
        
    }
    
    
    function inactiveRow(nRow, aData) {
          
        if(aData[9] == $statuses[1][1]) {  
            $(nRow).addClass("inactive");
            $('td:eq(8)', nRow).html($statuses[1][0]);
        } else {
            $(nRow).addClass("");
	    $('td:eq(8)', nRow).html($statuses[0][0]);
        }
        
	//Getting assigned value for each Service Provider for selected Network.
        if(aData[10]) {
	    $.post("{$_subdomain}/OrganisationSetup/serviceProviders/getAssigned/" + urlencode(aData[10]) + "/" + urlencode($("#nId").val()),        
	    '',      
	    function(data) {
		var p = eval("(" + data + ")");
		if(p) {   
		    $checked = ' checked="checked" ';
		} else {
		    $checked = '  ';
		}
		$checkbox = '<input type="checkbox" class="checkBoxDataTable" name="assignedSPs[]" ' + $checked + ' value="' + aData[10] + '" > ';
		$('td:eq(9)', nRow).html($checkbox);
            });
        }
        
    }
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                if($("#nId").val() != "") {
                    $showAssigned = '';
                    if($("#showAssigned").prop("checked"))
                    {
                        $showAssigned = 1;    
                    }
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/serviceProviders/'+urlencode($("#nId").val())+'/'+$showAssigned); 
                }
            }
        });
	$('input[placeholder], textarea[placeholder]').placeholder();


	$.validator.addMethod("time", function(value, element) {  
	    return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$/i.test(value);  
	}, "{$page['Errors']['time']|escape:'html'}");


                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });
                                
                  
                //Click handler for save changes button.
                $(document).on('click', '#save_changes_btn', 
                            function() {

                                sltSPs = '';

                                $('.checkBoxDataTable').each(function() {
                                    sltSPs += $(this).val()+',';
                                });

                                $("#sltSPs").val(sltSPs);  


                                $.post("{$_subdomain}/OrganisationSetup/serviceProviders/saveChanges/"+urlencode($("#nId").val()),        

                                    $("#ServiceProvidersResultsForm").serialize(),      
                                    function(data){

                                            var p = eval("(" + data + ")");

                                            if(p=="OK")
                                            {
                                                $("#centerInfoText").html("{$page['Text']['save_changes_done']|escape:'html'}").fadeIn('slow').delay("2000").fadeOut('slow');  
                                            }
                                            else 
                                            {
                                                $("#centerInfoText").html("{$page['Text']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow').delay("2000").fadeOut('slow');  
                                            }



                                    });

                            });   
                
                                
                                


                  
                  //Service Provider compnay address lookup starts here..
                  $(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');

                                            $('#PostalCode').focus();
                                    }
                                    else
                                    {
                                        var sow = $('#selectOutput').width();
                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        $('#selectOutput').width(sow);
                                        $("#postSelect").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                                   $('*').css('cursor','wait');
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                                    $('*').css('cursor','default');
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });                 
                //Service Provider compnay address lookup ends here..
                
                
                
                
                  //Invoiced compnay address lookup starts here..
                  $(document).on('click', '#quick_find_btn2', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#InvoicedPostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput2').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput2").slideDown("slow");
                                           

                                            $("#InvoicedBuilding").val('');	
                                            $("#InvoicedStreet").val('');
                                            $("#InvoicedArea").val('');
                                            $("#InvoicedTownCity").val('');

                                            $('#InvoicedPostalCode').focus();
                                    }
                                    else
                                    {
                                        var sow2 = $('#selectOutput2').width();
                                        $('#selectOutput2').html(data); 
                                        $("#selectOutput2").slideDown("slow");
                                        $('#selectOutput2').width(sow2);
                                         $("#postSelect_2").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                                   document.body.style.cursor = 'wait';
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    //setTimeout("$('#quickButton').fadeIn('medium')");
                                    document.body.style.cursor = 'default';
                            },
                            url:'{$_subdomain}/index/addresslookup/2' 
                        } ); 
                    return false;
                    
                    
                });                 
                //Invoiced compnay address lookup ends here..
                
                
                
                


                 /* Add a change handler to the county dropdown - strats here*/
                /*$(document).on('change', '#CountyID', 
                    function() {
                         
                         
                         $("#CountyID option:selected").each(function () {
                            $selected_cc_id =  $(this).attr('id');
                            $country_id_array = $selected_cc_id.split('_');
                            if($country_id_array[1]!='0')
                            {
                                $("#CountryID").val($country_id_array[1]);
                                $("#CountryID").blur();
                                $("#CountryID").attr("disabled","disabled").addClass('disabledField');
                            }
                            else
                            {
                                 $("#CountryID").val('');
                                 $("#CountryID").removeAttr("disabled").removeClass('disabledField');
                            }
                            
                            
                        });

                        
                        
                    }      
                );*/
              /* Add a change handler to the county dropdown - ends here*/
              
              
             /***************************************
             * Change handler for Country drop down *
             ***************************************/
             /*$(document).on('change', '#CountryID', 
                    function() {
                        var options = '';
                        var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                        
                        $('#VATRateID').empty()

                        $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                             
                        for (var n = 0; n < selectedCountryRates.length; n++) {
                            if (selectedCountryRates[n] !== undefined) {
                            options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                            }
                        }
                        
                        $('#VATRateID').append(options);
                    }      
                );*/
            

                    /* Add a change handler to the network dropdown - strats here*/
                    /*$(document).on('change', '#nId', 
                        function() {


                            $showAssigned = '';
                            if($("#showAssigned").prop("checked"))
                                {
                                    $showAssigned = 1;    
                                }


                            $(location).attr('href', '{$_subdomain}/OrganisationSetup/serviceProviders/'+urlencode($("#nId").val())+'/'+$showAssigned); 
                        }      
                    );*/
                   /* Add a change handler to the network dropdown - ends here*/
              


                   /* Add a click handler to the show assigned only check box - strats here*/
                        $(document).on('click', '#showAssigned', 
                            function() {
                                
                                 $showAssigned = '';
                                if($("#showAssigned").prop("checked"))
                                    {
                                        $showAssigned = 1;    
                                    }
                            
                            
                                $(location).attr('href', '{$_subdomain}/OrganisationSetup/serviceProviders/'+urlencode($("#nId").val())+'/'+$showAssigned); 
                            }      
                        );
                      /* Add a click handler to the show assigned only check box - ends here*/ 

               
                       



                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        


			{if $SupderAdmin == true} 
			    var displayButtons = "UAP";
			{else}
			    var displayButtons = "U";
			{/if} 
                         
                  
                        if($("#nId").val()) {
			
                            $("#showAssignedPanel").fadeIn();
                            $columnsList = [ 
				/* ServiceProviderID */ { bVisible: false },    
				/* CompanyName */	null,
				/* Acronym */		null,
				/* ASC */		null,
				/* Diary */		null,
				/* New Jobs */		null,
				/* Platform */		null,
				/* ServiceBaseVersion*/	null,
				/* IPAddress */		null,
				/* Status */		null,
				/* Assigned */		null
			    ];
                            $('#save_changes_btn').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
			    
                        } else {
			
                            $("#showAssignedPanel").fadeOut();
                            $columnsList = [ 
				/* ServiceProviderID */ { bVisible: false },    
				/* CompanyName */	null,
				/* Acronym */		null,
				/* ASC */		null,
				/* Diary */		null,
				/* New Jobs */		null,
				/* Platform */		null,
				/* ServiceBaseVersion*/	null,
				/* IPAddress */		null,
				/* Status */		null,
			    ];
			    $('#save_changes_btn').attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');                    
			    
                        }
     
                    
			$('#ServiceProvidersResults').PCCSDataTable( {
			    aoColumns:	    $columnsList,
                            aaSorting:	    [[ 1, "asc" ]],
                            displayButtons: displayButtons,
                            addButtonId:    'addButtonId',
                            addButtonText:  '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle:'{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:   '{$_subdomain}/OrganisationSetup/serviceProviders/insert/'+urlencode("{$nId}")+"/",
                            createDataUrl:  '{$_subdomain}/OrganisationSetup/ProcessData/ServiceProviders/',
                            createFormFocusElementId:'CompanyName',
                            formInsertButton:'insert_save_btn',
                            
                            
                            frmErrorRules:   {
                                                    
                                                    CompanyName:
                                                        {
                                                            required: true
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: function (element) {
                                                                if($("#CountryID").val()!='1')								    
                                                                {
                                                                    $("#PostCodeSup").hide();
                                                                    return false;
                                                                }
                                                                else
                                                                {
                                                                    $("#PostCodeSup").show();
                                                                    return true;
                                                                }
                                                            }
                                                        },
                                                    Street:
                                                        {
                                                            required: true
                                                        },
                                                    TownCity:
                                                        {
                                                            required: true
                                                        },
                                                    CountryID:
                                                        {
                                                            required: true
                                                        },    
                                                    ContactEmail:
                                                        {
                                                            required: true,
                                                            email:true
                                                        },
                                                    ReplyEmail:
                                                        {
                                                            email:true
                                                        },     
                                                    ContactPhone:
                                                        {
                                                            required: true
                                                        }, 
                                                    GeneralManagerEmail:
                                                        {
                                                            email:true
                                                        }, 
                                                    ServiceManagerEmail:
                                                        {
                                                            email:true
                                                        }, 
                                                    AdminSupervisorEmail:
                                                        {
                                                            email:true
                                                        },             
                                                 
                                                    Platform:
                                                        {
                                                            required: true
                                                        },  
                                                    IPAddress:
                                                        {
                                                            required: function (element) {
                                                                if($("#Platform").val()=='ServiceBase')
                                                                {
                                                                    $("#IPAddressSup").show();
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    $("#IPAddressSup").hide();
                                                                    return false;
                                                                }
                                                            }
                                                        },  
                                                    Port:
                                                        {
                                                            required: function (element) {
                                                                if($("#Platform").val()=='ServiceBase')
                                                                {
                                                                    $("#PortSup").show();
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    $("#PortSup").hide();
                                                                    return false;
                                                                }
                                                            },
                                                            digits: true
                                                        },
                                                    WeekdaysOpenTime:
                                                        {
                                                            time: true
                                                        },
                                                    WeekdaysCloseTime:
                                                        {
                                                            time: true
                                                        },
                                                    SaturdayOpenTime:
                                                        {
                                                            time: true
                                                        },
                                                    SaturdayCloseTime:
                                                        {
                                                            time: true
                                                        },
                                                    SundayOpenTime:
                                                        {
                                                            time: true
                                                        },
                                                    SundayCloseTime:
                                                        {
                                                            time: true
                                                        },            
                                                    DefaultTravelTime:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        },
                                                    DefaultTravelSpeed:
                                                        {
                                                            required: true,
                                                            digits: true,
                                                            max: 200,
                                                            min:50
                                                        },
						    Acronym: { required: true },
						    ServiceManagerEmail: { email: true },
						    AdminSupervisorEmail: { email: true }
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    CompanyName:
                                                        {
                                                            required: "{$page['Errors']['company_name']|escape:'html'}"
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: "{$page['Errors']['post_code']|escape:'html'}"
                                                        },
                                                    Street:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"
                                                        },
                                                    TownCity:
                                                        {
                                                            required: "{$page['Errors']['town']|escape:'html'}"
                                                        },
                                                    CountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"
                                                        },       
                                                    ContactEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },
                                                     ReplyEmail:
                                                        {
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },   
                                                        
                                                    ContactPhone:
                                                        {
                                                            required: "{$page['Errors']['phone']|escape:'html'}"
                                                        }, 
                                                                
                                                     GeneralManagerEmail:
                                                        {
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        }, 
                                                    ServiceManagerEmail:
                                                        {
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        }, 
                                                    AdminSupervisorEmail:
                                                        {
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },            
                                                                
                                                    Platform:
                                                        {
                                                            required: "{$page['Errors']['platform']|escape:'html'}"
                                                        },  
                                                    IPAddress:
                                                        {
                                                            required: "{$page['Errors']['ip_address']|escape:'html'}"
                                                        },  
                                                    Port:
                                                        {
                                                            required: "{$page['Errors']['port']|escape:'html'}",
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    WeekdaysOpenTime:
                                                        {
                                                            time: "{$page['Errors']['weekdays_open_time_invalid']|escape:'html'}"
                                                        },
                                                    WeekdaysCloseTime:
                                                        {
                                                            time: "{$page['Errors']['weekdays_close_time_invalid']|escape:'html'}"
                                                        },
                                                    SaturdayOpenTime:
                                                        {
                                                            time: "{$page['Errors']['saturday_open_time_invalid']|escape:'html'}"
                                                        },
                                                    SaturdayCloseTime:
                                                        {
                                                            time: "{$page['Errors']['saturday_close_time_invalid']|escape:'html'}"
                                                        },
                                                    SundayOpenTime:
                                                        {
                                                            time: "{$page['Errors']['sunday_open_time_invalid']|escape:'html'}"
                                                        },
                                                    SundayCloseTime:
                                                        {
                                                            time: "{$page['Errors']['sunday_close_time_invalid']|escape:'html'}"
                                                        },            
                                                                
                                                    DefaultTravelTime:
                                                        {
                                                            required: "{$page['Errors']['default_travel_time']|escape:'html'}",
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    DefaultTravelSpeed:
                                                        {
                                                            required: "{$page['Errors']['default_travel_speed']|escape:'html'}",
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
						    Acronym: { required: "{$page['Errors']['acronym']|escape:'html'}" },
						    ServiceManagerEmail: { email: "{$page['Errors']['valid_email']|escape:'html'}" },
						    AdminSupervisorEmail: { email: "{$page['Errors']['valid_email']|escape:'html'}" }
                                                     
                                              },                     
                            
                            popUpFormWidth:  800,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/serviceProviders/update/' + urlencode("{$nId}") + "/",
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/ServiceProviders/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'CompanyName',
                            
                            colorboxFormId:  "ServiceProvidersForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'ServiceProvidersResultsPanel',
                            htmlTableId:     'ServiceProvidersResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/ServiceProviders/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$showAssigned}"),
                            formCancelButton:'cancel_btn',
			    //pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            pickCallbackMethod: 'sendSettings',
                            pickButtonStyle: 'style="width:220px"',
                            pickButtonText:"{$page['Buttons']['send_settings']|escape:'html'}",
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command',
                            colorboxForceClose: false,
                            insertBeforeSendMethod: "ajaxFileUpload",
                            updateBeforeSendMethod: "ajaxFileUpload",
                            onSuccess: function( mode, data ) { 
                                           {* update AuthMan on success only if INSERT mode... *}
                                           //console.log('mode = '+mode);
                                           if (mode == 1) PostAuthMan( data['id'] ); 
                                       }

                        });
                      




          $(document).on('click', '#AddClientDetails', 
                                function() {
                            
                        
                       if($("#ClientID").val()!='' && ($("#ClientContactPhone").val()!='' || $("#ClientContactEmail").val()!='')) 
                       {
                               
                               var newRow = "<tr id='C_"+$("#ClientID").val()+"'  ><td><input type='hidden' name='ClientIDs[]' value='"+$("#ClientID").val()+"' >"+$('#ClientID option:selected').html()+"</td><td><input type='hidden' name='ClientContactPhones[]' value='"+$("#ClientContactPhone").val()+"' >"+$("#ClientContactPhone").val()+"</td><td><input type='hidden' name='ClientContactEmails[]' value='"+$("#ClientContactEmail").val()+"' >"+$("#ClientContactEmail").val()+"</td><td><span><img style='cursor:pointer;' class='CloseImage'  src='{$_subdomain}/css/Skins/{$_theme}/images/close.png' width='17' height='16' ></span></td></tr>";
                               
                              
                               
                               
                               $("#ClientsList").append(newRow);
                               
                               $("#ClientID option[value='"+$("#ClientID").val()+"']").hide();
                               
                               $("#ClientID").val('');
                               $("#ClientContactPhone").val('');
                               $("#ClientContactEmail").val('');
                               
                       }
                       else
                       {
                               alert('{$page['Errors']['client_contact']|escape:'html'}');
                       }
                       
                       
                
               });
             
             
             
             
             
              $(document).on('click', '.CloseImage', 
                                function() {
                              
                          if(confirm('{$page['Errors']['delete_client_contact']|escape:'html'}'))    
                          {    
                            $row_id = $(this).closest('tr').attr("id");

                            $row_id =  $row_id.replace("C_",""); 

                            $("#ClientID option[value='"+$row_id+"']").show();      

                            $(this).closest('tr').remove();   
                         }
                
               });                  



                   

    });

</script>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ServiceProvidersTopForm" name="ServiceProvidersTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="ServiceProvidersResultsPanel" >
                    
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm" class="nidCorrections">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        <span id="showAssignedPanel" style="display:none;" >
                        <input class="assignedTextBox" id="showAssigned"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        
                        
                        </form>
                    {/if} 
                    
                    
                    <form id="ServiceProvidersResultsForm" class="dataTableCorrections">
                    <table id="ServiceProvidersResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
				<th></th> 
				<th width="24%" title="{$page['Text']['name']|escape:'html'}">
				    {$page['Text']['name']|escape:'html'}
				</th>
				<th width="8%" title="{$page['Labels']['acronym']|escape:'html'}">
				    {$page['Labels']['acronym']|escape:'html'}
				</th>
				<th width="5%" title="Email Individual ASC's the Daily Network Opens Jobs Report">
				    {$page['Labels']['asc']|escape:'html'}
				</th>
				<th width="7%" title="{$page['Labels']['diary']|escape:'html'}">
				    {$page['Labels']['diary']|escape:'html'}
				</th>
				<th width="8%" title="{$page['Labels']['new_jobs']|escape:'html'}">
				    {$page['Labels']['new_jobs']|escape:'html'}
				</th>
				<th width="8%" title="{$page['Text']['platform']|escape:'html'}">
				    {$page['Text']['platform']|escape:'html'}
				</th>
				<th width="15%" title="{$page['Text']['servicebase_version']|escape:'html'}">
				    {$page['Text']['servicebase_version']|escape:'html'}
				</th>
				<th width="11%" title="{$page['Text']['ip_address']|escape:'html'}">
				    {$page['Text']['ip_address']|escape:'html'}
				</th>
				<th width="6%" title="{$page['Text']['status']|escape:'html'}">
				    {$page['Text']['status']|escape:'html'}
				</th>
				{if $nId != ''}
				    <th title="{$page['Text']['assigned']|escape:'html'}">
					{$page['Text']['assigned']|escape:'html'}
				    </th>
				{/if}
			    </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                    <input type="hidden" name="sltSPs" id="sltSPs" >            
                    </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    <button id="save_changes_btn" type="button" class="gplus-blue-disabled leftBtn" ><span class="label">{$page['Buttons']['save_changes']|escape:'html'}</span></button>
                   
                     <span id="processDisplayText" style="color:red;display:none;padding-left:250px;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                     
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        
                <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> 

                

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



