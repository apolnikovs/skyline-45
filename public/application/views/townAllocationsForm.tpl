{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
    <script type="text/javascript" >
              
        
    $(document).ready(function() {
        $("#NetworkID").combobox({
            change: function() {
                //Triggering job type to get service types.
                // $("#JobTypeID").trigger("change");    
                 $manufacturerDropDownList = $clientDropDownList = $serviceProviderDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                 var $NetworkID = $("#NetworkID").val();
                 if($NetworkID && $NetworkID!='')
                 {
                    //Getting clients for selected network.   
                    $.post("{$_subdomain}/JobAllocation/townAllocations/getClients/"+urlencode($NetworkID), '', function(data){
                        var $networkClients = eval("(" + data + ")");
                        if($networkClients)
                        {
                            for(var $i=0;$i<$networkClients.length;$i++)
                            {
                                $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                            }
                        }
                        $("#ClientID").html('');
                        $("#ClientID").html($clientDropDownList);
                    });
                    //Getting manufacturer for selected network.   
                    $.post("{$_subdomain}/JobAllocation/townAllocations/getManufacturers/"+urlencode($NetworkID), '', function(data){
                        var $networkManufacturers = eval("(" + data + ")");
                        if($networkManufacturers)
                        {
                            for(var $i=0;$i<$networkManufacturers.length;$i++)
                            {
                                $manufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                            }
                        }
                        $("#ManufacturerID").html('');
                        $("#ManufacturerID").html($manufacturerDropDownList);
                     });
                    //Getting service providers for selected network.   
                    $.post("{$_subdomain}/JobAllocation/townAllocations/getServiceProviders/"+urlencode($NetworkID), '', function(data){
                        var $networkServiceProviders = eval("(" + data + ")");
                        if($networkServiceProviders)
                        {
                            for(var $i=0;$i<$networkServiceProviders.length;$i++)
                            {
                                $serviceProviderDropDownList += '<option value="'+$networkServiceProviders[$i]['ServiceProviderID']+'" >'+$networkServiceProviders[$i]['CompanyName']+'</option>';
                            }
                        }
                        $("#ServiceProviderID").html('');
                        $("#ServiceProviderID").html($serviceProviderDropDownList);
                     });
                 }
                 else 
                 {
                     $("#ClientID").html('');
                     $("#ManufacturerID").html('');
                     $("#ServiceProviderID").html('');
                     $("#ClientID").html($clientDropDownList);
                     $("#ManufacturerID").html($manufacturerDropDownList);
                     $("#ServiceProviderID").html($serviceProviderDropDownList);
                 }
            }
        });
        var $PrePopulatedCountryID = $("#CountryID").val();
        if($PrePopulatedCountryID)
        {
            $countyDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
            $.post("{$_subdomain}/JobAllocation/townAllocations/getCounties/"+urlencode($PrePopulatedCountryID), '', function(data){
                var $counties = eval("(" + data + ")");
                if($counties)
                {
                    for(var $i=0;$i<$counties.length;$i++)
                    {
                        $countyDropDownList += '<option value="'+$counties[$i]['CountyID']+'" >'+$counties[$i]['Name']+'</option>';
                    }
                }
                $("#CountyID").html('');    
                $("#CountyID").html($countyDropDownList);
            });
        }
        $("#CountryID").combobox({
            change: function() {
                $countyDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                var $CountryID      = $("#CountryID").val();
                if($CountryID)
                {
                    //Getting counties for selected country.   
                    $.post("{$_subdomain}/JobAllocation/townAllocations/getCounties/"+urlencode($CountryID), '', function(data){
                        var $counties = eval("(" + data + ")");
                        if($counties)
                        {
                            for(var $i=0;$i<$counties.length;$i++)
                            {
                                $countyDropDownList += '<option value="'+$counties[$i]['CountyID']+'" >'+$counties[$i]['Name']+'</option>';
                            }
                        }
                        $("#CountyID").html('');    
                        $("#CountyID").html($countyDropDownList);
                    });
                }
                else 
                {
                    $("#CountyID").html('');
                    $("#CountyID").html($countyDropDownList);
                }
            }
        });
        $("#ManufacturerID").combobox();
        $("#CountyID").combobox();
        $("#RepairSkillID").combobox();
        $("#ClientID").combobox({
            change: function() {
                $("#JobTypeID").trigger("change");
            }
        });
        $("#JobTypeID").combobox({
            change: function() {
                $serviceTypeDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                var $JobTypeID = $("#JobTypeID").val();
                if($JobTypeID && $JobTypeID!='')
                {
                    //Getting service types for selected network, client and jobtype.   
                    $.post("{$_subdomain}/JobAllocation/townAllocations/getServiceTypes/"+urlencode($JobTypeID)+"/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ClientID").val()), '', function(data){
                        var $serviceTypes = eval("(" + data + ")");
                        if($serviceTypes)
                        {
                            for(var $i=0;$i<$serviceTypes.length;$i++)
                            {
                                $serviceTypeDropDownList += '<option value="'+$serviceTypes[$i]['ServiceTypeID']+'" >'+$serviceTypes[$i]['Name']+'</option>';
                            }
                        }
                        $("#ServiceTypeID").html('');
                        $("#ServiceTypeID").html($serviceTypeDropDownList);
                    });
                }
                else 
                {
                    $("#ServiceTypeID").html('');
                    $("#ServiceTypeID").html($serviceTypeDropDownList);
                }
                $("#RepairSkillID").trigger("change");
            }
        });
        $("#ServiceTypeID").combobox();
        $("#ServiceProviderID").combobox();
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
                //change handler for network dropdown box.
                /*$(document).on('change', '#NetworkID', 
                            function() {

                                    //Triggering job type to get service types.
                                   // $("#JobTypeID").trigger("change");    

                                 
                                    $manufacturerDropDownList = $clientDropDownList = $serviceProviderDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                    var $NetworkID = $("#NetworkID").val();

                                    if($NetworkID && $NetworkID!='')
                                    {
                                       
                                       //Getting clients for selected network.   
                                       $.post("{$_subdomain}/JobAllocation/townAllocations/getClients/"+urlencode($NetworkID),        

                                        '',      
                                        function(data){
                                                var $networkClients = eval("(" + data + ")");
    
                                                if($networkClients)
                                                {
                                                     
                                                    for(var $i=0;$i<$networkClients.length;$i++)
                                                    {
                                                        
                                                        $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                    }
                                                }
                                                
                                                
                                                $("#ClientID").html('');
                                                $("#ClientID").html($clientDropDownList);

                                        });
                                        
                                        
                                        
                                       //Getting manufacturer for selected network.   
                                       $.post("{$_subdomain}/JobAllocation/townAllocations/getManufacturers/"+urlencode($NetworkID),        

                                        '',      
                                        function(data){
                                                var $networkManufacturers = eval("(" + data + ")");
    
                                                if($networkManufacturers)
                                                {
                                                     
                                                    for(var $i=0;$i<$networkManufacturers.length;$i++)
                                                    {
                                                        
                                                        $manufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                                                    }
                                                }
                                                
                                                 $("#ManufacturerID").html('');
                                                
                                                $("#ManufacturerID").html($manufacturerDropDownList);

                                        });
                                        
                                        
                                        
                                       //Getting service providers for selected network.   
                                       $.post("{$_subdomain}/JobAllocation/townAllocations/getServiceProviders/"+urlencode($NetworkID),        

                                        '',      
                                        function(data){
                                                var $networkServiceProviders = eval("(" + data + ")");
    
                                                if($networkServiceProviders)
                                                {
                                                     
                                                    for(var $i=0;$i<$networkServiceProviders.length;$i++)
                                                    {
                                                        
                                                        $serviceProviderDropDownList += '<option value="'+$networkServiceProviders[$i]['ServiceProviderID']+'" >'+$networkServiceProviders[$i]['CompanyName']+'</option>';
                                                    }
                                                }
                                                
                                                $("#ServiceProviderID").html('');
                                                $("#ServiceProviderID").html($serviceProviderDropDownList);

                                        });
                                        
                                        
                                    }
                                    else 
                                    {
                                        $("#ClientID").html('');
                                        $("#ManufacturerID").html('');
                                        $("#ServiceProviderID").html('');
                        
                                        $("#ClientID").html($clientDropDownList);
                                        $("#ManufacturerID").html($manufacturerDropDownList);
                                        $("#ServiceProviderID").html($serviceProviderDropDownList);
                                    }
                                   
                                               

                            });*/    
                            
                            
                //change handler for job type dropdown box.
                /*$(document).on('change', '#JobTypeID', 
                            function() { 
                            
                                   $serviceTypeDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                    var $JobTypeID = $("#JobTypeID").val();

                                    if($JobTypeID && $JobTypeID!='')
                                    {
                                       
                                       //Getting service types for selected network, client and jobtype.   
                                       $.post("{$_subdomain}/JobAllocation/townAllocations/getServiceTypes/"+urlencode($JobTypeID)+"/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ClientID").val()),        

                                        '',      
                                        function(data){
                                                var $serviceTypes = eval("(" + data + ")");
    
                                                if($serviceTypes)
                                                {
                                                     
                                                    for(var $i=0;$i<$serviceTypes.length;$i++)
                                                    {
                                                        
                                                        $serviceTypeDropDownList += '<option value="'+$serviceTypes[$i]['ServiceTypeID']+'" >'+$serviceTypes[$i]['Name']+'</option>';
                                                    }
                                                }
                                                
                                                $("#ServiceTypeID").html('');
                                                
                                                $("#ServiceTypeID").html($serviceTypeDropDownList);

                                        });
                                        
                                        
                                    }
                                    else 
                                    {
                                        $("#ServiceTypeID").html('');
                                        
                                        $("#ServiceTypeID").html($serviceTypeDropDownList);
                                        
                                    }
                            
                            
                                    $("#RepairSkillID").trigger("change");
                            
                             });*/  
                             
                             
                  //change handler for client dropdown box.
                  /*$(document).on('change', '#ClientID', 
                            function() {
                            
                                $("#JobTypeID").trigger("change");
                              
                             });*/  
                             
                             
                      
                  //change handler for country dropdown box.
                  /*$(document).on('change', '#CountryID', 
                            function() {
                            
                                
                                $countyDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                var $CountryID      = $("#CountryID").val();
                              
                                if($CountryID)
                                {

                                    //Getting counties for selected country.   
                                    $.post("{$_subdomain}/JobAllocation/townAllocations/getCounties/"+urlencode($CountryID),        

                                    '',      
                                    function(data){
                                            var $counties = eval("(" + data + ")");

                                            if($counties)
                                            {

                                                for(var $i=0;$i<$counties.length;$i++)
                                                {

                                                    $countyDropDownList += '<option value="'+$counties[$i]['CountyID']+'" >'+$counties[$i]['Name']+'</option>';
                                                }
                                            }

                                            $("#CountyID").html('');    
                                            $("#CountyID").html($countyDropDownList);

                                    });


                                }
                                else 
                                {
                                    $("#CountyID").html('');
                                    $("#CountyID").html($countyDropDownList);

                                }
                                
                                
                             });*/            
                            
                            
                
         });       
        
    </script>
    
    
    <div id="TownAllocationsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="TownAllocationsForm" name="TownAllocationsForm" method="post" action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                        <p>
                               <label ></label>
                               <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                        </p>
                       
                         
                         
                        
                        
                        {if $SupderAdmin eq true} 
                         
                          
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                        {else}
                        
                         <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                             &nbsp;&nbsp;  
                            <label class="labelHeight"  >  
                             {$datarow.NetworkName|escape:'html'}
                             <input type="hidden" name="NetworkID" id="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                            </label> 
                          </p>
                          
                        {/if}    
                        
                        <p>
                                <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;  
                                <select name="CountryID" id="CountryID" class="text" >
                                <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                {/foreach}

                                </select>
                        </p>

                        
                        
                        <p>
                                <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;  
                                <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if $datarow.ManufacturerID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if $datarow.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                        
                        
                         <p>
                                <label class="fieldLabel" for="CountyID" >{$page['Labels']['county']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;  
                                <select name="CountyID" id="CountyID" class="text" >
                                <option value="" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $counties as $county}

                                    <option value="{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                        
                        
                         
                         <p>
                            <label class="fieldLabel" for="Town" >{$page['Labels']['town']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Town" value="{$datarow.Town|escape:'html'}" id="Town" >          
                        </p>

                         
                        
                        <p>
                                <label class="fieldLabel" for="RepairSkillID" >{$page['Labels']['repair_skill']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="RepairSkillID" id="RepairSkillID" class="text" >
                                <option value="" {if $datarow.RepairSkillID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $repairSkills as $repairSkill}

                                    <option value="{$repairSkill.RepairSkillID}" {if $datarow.RepairSkillID eq $repairSkill.RepairSkillID}selected="selected"{/if}>{$repairSkill.RepairSkillName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                        
                        
                         <p>
                                <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                         
                         
                         
                         
                         
                         
                         <p>
                                <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['job_type']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="JobTypeID" id="JobTypeID" class="text" >
                                <option value="" {if $datarow.JobTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $jobTypes as $jobType}

                                    <option value="{$jobType.JobTypeID}" {if $datarow.JobTypeID eq $jobType.JobTypeID}selected="selected"{/if}>{$jobType.Type|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                         
                         
                         
                         <p>
                                <label class="fieldLabel" for="ServiceTypeID" >{$page['Labels']['service_type']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="ServiceTypeID" id="ServiceTypeID" class="text" >
                                <option value="" {if $datarow.ServiceTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $serviceTypes as $serviceType}

                                    <option value="{$serviceType.ServiceTypeID}" {if $datarow.ServiceTypeID eq $serviceType.ServiceTypeID}selected="selected"{/if}>{$serviceType.Name|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                         
                         
                         
                          <p>
                                <label class="fieldLabel" for="ServiceProviderID" >{$page['Labels']['service_centre']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="ServiceProviderID" id="ServiceProviderID" class="text" >
                                <option value="" {if $datarow.ServiceProviderID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $serviceProviders as $serviceProvider}

                                    <option value="{$serviceProvider.ServiceProviderID}" {if $datarow.ServiceProviderID eq $serviceProvider.ServiceProviderID}selected="selected"{/if}>{$serviceProvider.CompanyName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                       
                        
                             
                        <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; 

                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                        </p>
                            
                         

                        <p>

                            <span class= "bottomButtons" >

                                <input type="hidden" name="TownAllocationID"  value="{$datarow.TownAllocationID|escape:'html'}" >


                                {if $datarow.TownAllocationID neq '' && $datarow.TownAllocationID neq '0'}

                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {else}

                                    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {/if}

                                    <br>
                                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                            </span>

                        </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
