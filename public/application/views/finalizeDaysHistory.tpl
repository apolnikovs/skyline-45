<script type="text/javascript">
    $(document).ready(function() {
        $('#finalisedDayHistoryResults').PCCSDataTable({
            sDom: 'ft<"#dataTables_child">rpli',
            oLanguage: { "sEmptyTable": "No Appointments" },
            bServerSide: false,
            bStateSave: true,
            htmlTableId: 'finalisedDayHistoryResults',
            htmlTablePageId: 'finalisedDayHistory',
            fetchDataUrl: '{$_subdomain}/AppointmentDiary/getFinalisedDayHistoryDetails/'+urlencode('{$rDate}'),
            aaSorting: [[ 0, "asc" ]],
            aoColumns: [ 
                { sType: "date-eu" },
                null,
                null,
                { bSortable : false },
                { bSortable : false }
            ],
            bottomButtonsDivId:'dataTables_child',
            searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',        
            popUpFormWidth:  0,
            popUpFormHeight: 0,
            bDestroy:true
        });
        $("#finalisedDayHistoryResults_filter").hide();
    });
</script>
<div  id="finalisedDayHistory" class="ServiceProvidersPage">   
    <form id="finalisedDaysForm" name="finalisedDaysForm" method="post"  action="#" class="inline" >
        <fieldset>
            <legend> {$page['Text']['finalisedday_history_page_legend']|escape:'html'} </legend>
            <table id="finalisedDayHistoryResults" border="0" cellpadding="0" cellspacing="0" class="browse">
                <thead>
                <tr>
                <th>{$page['Text']['appointment_date']|escape:'html'}</th>
                <th>{$page['Text']['user_name']|escape:'html'}</th>
                <th>{$page['Text']['action']|escape:'html'}</th>
                <th>{$page['Text']['brown_goods']|escape:'html'}</th>
                <th>{$page['Text']['white_goods']|escape:'html'}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>  
        </fieldset>
    </form>
</div>