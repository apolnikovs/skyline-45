<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.form_input.php
 * Type:        function
 * Name:        form_input
 * Description: creates a form element.
 *
 * -------------------------------------------------------------
 */

function smarty_function_input($params, &$smarty) {
    
    require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');

    $name = null;
    $label = null;
    $help = null;
    $value = null;
    $maxlen = null;
    $rows = null;
    $options = null;
    $required = false;
    $error = null;
    $tab = null;
    $type = 'text';
    $tabindex = '';
    $buttons = 'reset,submit=OK';

    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'data':
                $name = (string) $_val['name'];
                $error = smarty_function_escape_special_chars($_val['error']);
                $value = smarty_function_escape_special_chars($_val['value']);
                if (is_bool($_val['required'])) {
                    $required = $_val['required'];
                } else {
                    trigger_error("form: required is not true/false", E_USER_NOTICE);
                }
                if (is_numeric($_val['maxlen'])) {
                    $maxlen = (string) $_val['maxlen'];
                } else {
                    trigger_error("form: maxlen is not numeric", E_USER_NOTICE);
                }
                if (is_array($_val['options'])) {
                    $options = (array) $_val['options'];
                } //else {
                //    $options = array();
                //}
                break;
            case 'label':
                $label = smarty_function_escape_special_chars($_val);
                break;
            case 'help':
                $help = smarty_function_escape_special_chars($_val);
                break;
            case 'tab':
                if (is_numeric($_val)) {
                    $tabindex = " tabindex=\"$_val\"";
                    $tab = $_val;
                } else {
                    trigger_error("form: tab is not numeric", E_USER_NOTICE);
                }
                break;
            case 'rows':
                if (is_numeric($_val)) {
                    $rows = (string) $_val;
                } else {
                    trigger_error("form: rows is not numeric", E_USER_NOTICE);
                }
                break;
            case 'type':
                $type = (string) $_val;
                break;
            case 'buttons':
                $type = 'buttons';
                $buttons = (string) $_val;
                break;
            default:
                trigger_error("form: unrecognised attribute '$_key' ", E_USER_NOTICE);
                break;
        }
    }

    $html = '';
    
    switch ($type) {
            case 'text':
                $html = render_input($name,'text',$label,$help,$value,$maxlen,$rows,$required,$error,$tabindex);
                break;
            case 'password':
                $html = render_input($name,'password',$label,$help,$value,$maxlen,null,$required,$error,$tabindex);
                break;
            case 'date':
                $html = render_input($name,'date',$label,$help,$value,0,null,$required,$error,$tabindex);
                break;
            case 'display':
                $html = render_display($name,$label,$value,$rows);
                break;
            case 'hidden':
                $html = render_hidden($name,$value);
                break;
            case 'combo':
                $html = render_combo($name,$label,$help,$value,$options,$required,$error,$tabindex);
                break;
            case 'checkbox':
                $html = render_checkbox($name,$label,$help,$value,$options,$required,$error,$tabindex);
                break;
            case 'radio-buttons':
                $html = render_radiobuttons($name,$label,$help,$value,$options,$required,$error,$tabindex);
                break;
            case 'buttons':
                $html = render_buttons($buttons,$tab);
                break;
            default:
                trigger_error("form: unrecognised type attribute '$_val' ", E_USER_NOTICE);
                break;
        }
        
        return $html;
        
}

function render_div($label, $name, $help, $required, $error) {

    if ($error == '') {
        $html = "<p>\n";
    } else {
        $html = "<p class=\"error\">\n";
        $html .= "<strong>{$error}</strong>\n";
    }
    $html .= "<label for=\"{$name}\">{$label}";
    if ($required) {
        $html .= "<sup title=\"This field is mandatory.\">*</sup>";
    }
    if (isset($help)) {
        $html .= "<br />\n<span>{$help}</span>\n</label>\n";
    } else {
        $html .= "</label>\n";
    }

    return $html;
}

function render_input($name,$type,$label,$help,$value,$maxlen,$rows,
                         $required,$error,$tabindex) {
    
    if (!isset($label)) {
        trigger_error("form: label is missing ", E_USER_NOTICE);
    }

    $maxlen_attribute = '';
    $date_attribute = '';

    if ($type == 'date') {
        $type = 'text';
        $date_attribute = " class=\"datepicker\"";
    }

    if($maxlen != '0') {
        $maxlen_attribute = " maxlength=\"{$maxlen}\"";
    }

    $html = render_div($label, $name, $help, $required, $error);

    if($value == '') {
        if (isset($rows)) {
            $html .= "<textarea name=\"{$name}\" id=\"{$name}\" rows=\"{$rows}\"{$maxlen_attribute}{$tabindex}>\n</textarea>\n";
        } else {
            $html .= "<input type=\"{$type}\" class=\"text\" name=\"{$name}\" id=\"{$name}\"{$maxlen_attribute}{$date_attribute}{$tabindex} />\n";
        }
    } else {
        if (isset($rows)) {
            $html .= "<textarea name=\"{$name}\" id=\"{$name}\" rows=\"{$rows}\"{$maxlen_attribute}{$tabindex}>\n{$value}\n</textarea>\n";
        } else {
            $html .= "<input type=\"{$type}\" class=\"text\" name=\"{$name}\" id=\"{$name}\" value=\"{$value}\"{$maxlen_attribute}{$date_attribute}{$tabindex} />\n";
        }
    }

    $html .= "</p>\n";

    return $html;

}

function render_hidden($name,$value) {
    
    if (!isset($name)) {
        trigger_error("form: name is missing ", E_USER_NOTICE);
    }

    $html = "<input type=\"hidden\" name=\"{$name}\" id=\"{$name}\"  value=\"{$value}\" />\n";

    return $html;
}

function render_display($name,$label,$value,$rows) {
    
    if (!isset($label)) {
        trigger_error("form: label is missing ", E_USER_NOTICE);
    }

    if ($value == '') {
        $value = '&nbsp;';
    }

    $html = "<p>\n";
    $html .= "<label>{$label}</label>\n";
    if (isset($rows)) {
        $html .= "<textarea rows=\"{$rows}\" disabled=\"disabled\" class=\"disabled\">\n{$value}\n</textarea>\n";
    } else {
        $html .= "<input type=\"text\" value=\"{$value}\" disabled=\"disabled\" class=\"disabled\" />\n";
    }
    $html .= "</p>\n";

    return $html;
}

function render_combo($name,$label,$help,$value,$options,
                         $required,$error,$tabindex) {
    
    if (!isset($label)) {
        trigger_error("form: label is missing ", E_USER_NOTICE);
    }

    if (!isset($options)) {
        trigger_error("form: options are missing ", E_USER_NOTICE);
    }

    $html = render_div($label, $name, $help, $required, $error);

    $html .= "<select name=\"{$name}\" id=\"{$name}\" size=\"1\"{$tabindex}>\n";

    if ($selected == '') {
        if ($required) {
            $html .="<option value=\"\" selected=\"selected\" disabled=\"disabled\">Please choose</option>\n";
        } else {
            $html .="<option value=\"\" selected=\"selected\">Please choose</option>\n";
        }
    }

    foreach ($options as $_key => $_val) {
        $html .= generate_option($_key, $_val, $selected);
    }

    $html .= "</select>\n</p>\n";
    return $html;
}

function render_checkbox($name,$label,$help,$value,$options,
                         $required,$error,$tabindex) {
    
    if (!isset($label)) {
        trigger_error("form: label is missing ", E_USER_NOTICE);
    }
    if ($value == '') {
        $value = $name;
    }

    if ($error == '') {
        $html = "<p class=\"checkbox\">\n";
    } else {
        $html = "<p class=\"checkbox error\">\n";
        $html .= "<strong>{$error}</strong>\n";
    }

    $html .= "<input type=\"checkbox\" name=\"{$name}\" id=\"{$name}\" value=\"{$value}\"{$checked}{$tabindex} />\n";

    $html .= "<label for=\"{$name}\">{$label}";
    if ($required) {
        $html .= "<sup title=\"This field is mandatory.\">*</sup>";
    }
    if (isset($help)) {
        $html .= "<br />\n<span>{$help}<span>\n</label>\n";
    } else {
        $html .= "</label>\n";
    }


    $html .= "</p>\n";
    return $html;
}

function render_radiobuttons($name,$label,$help,$value,$maxlen,$rows,
                         $required,$error,$tabindex) {
    
    if (!isset($label)) {
        trigger_error("form: label is missing ", E_USER_NOTICE);
    }

    if (!isset($options)) {
        trigger_error("form: options are missing ", E_USER_NOTICE);
    }

    $html = "<fieldset>\n<legend>{$label}";

    if ($required) {
        $html .= "<sup title=\"This field is mandatory.\">*</sup>";
    }

    if (isset($help)) {
        $html .= "\n<span>{$help}<span>\n";
    }

    $html .= "</legend>\n";

    if ($error == ''){
        $html .= "<p class=\"checkbox\">\n";
    } else {
        $html .= "<div class=\"checkbox error\" role=\"alert\" aria-live=\"assertive\">\n";
        $html .= "<strong class=\"message\">{$error}</strong>\n";
    }

    $id = 0;
    foreach($options as $key => $val) {
        $id++;
        $html .= generate_radiobutton($name, $id, $key, $val, $selected);
    }


    $html .= "</p>\n</fieldset>\n";
    return $html;
}

function render_buttons($buttonlist,$tab) {
    
    $buttons = explode(',', $buttonlist);
    $html = "<p>\n";
    $tabindex = '';
    foreach ( $buttons as $button ) {
        if (is_numeric($tab)) {
            $tabindex = " tabindex=\"$tab\"";
            $tab++;
        }
        $html .= render_button($button, $tabindex);
    }
    $html .= "</p>\n";

    return $html;

}

function render_button($type, $tabindex) {
    
    $pos = strpos($type, '=');
    if ($pos === false) {
        $value = ucwords($type);
    } else {
        $value = substr($type, $pos+1);
        $type = substr($type, 0, $pos);
    }
    switch ($type) {
        case 'submit':
            return "<input type=\"submit\" value=\"{$value}\" class=\"button\" id=\"button_submit\" name=\"button_submit\"{$tabindex} />\n";
        case 'reset':
            return "<input type=\"reset\" value=\"{$value}\" class=\"button\" id=\"button_reset\" name=\"button_reset\"{$tabindex} />\n";
        default:
            return "<input type=\"button\" value=\"{$value}\" class=\"button\" id=\"button_{$type}\" name=\"button_{$type}\"{$tabindex} />\n";

    }

}

function render_radiobutton($group, $id, $label, $value, $selected) {

    // <div>
    //     <input type="radio" name="vote" value="Option 1" id="vote1" />
    //     <label for="vote1">Option 1</label>
    //  </div>

    $name = smarty_function_escape_special_chars($name);
    $label = smarty_function_escape_special_chars($label);
    $checked = ($value == $selected) ? ' checked' : '';

    $rb = "<p>\n";
    $rb .= "    <input type=\"radio\" name=\"{$group}\" id=\"{$group}_{$id}\" value=\"{$value}\"{$checked} />\n";
    $rb .= "    <label for=\"{$group}_{$id}\">{$label}</label>\n";
    $rb .= "</p>\n";

    return $rb;

}

function render_option($name, $val, $selected) {
    $opt = '';
    $name_encoded = smarty_function_escape_special_chars($name);
    if (is_array($val)) {
        $opt .= "<optgroup label=\"{$name_encoded}\">";
        foreach ($val as $_key => $_val) {
            $opt .= generate_option($_key, $_val, $selected);
        }
        $opt .= "</optgroup>";
    } else {
        $val_encoded= smarty_function_escape_special_chars($val);
        if($val == $selected) {
            $opt .= "<option value=\"{$val_encoded}\" selected=\"selected\">{$name_encoded}</option>\n";
        } else {
            $opt .= "<option value=\"{$val_encoded}\">{$name_encoded}</option>\n";
        }
    }
    return $opt;
}

?>
